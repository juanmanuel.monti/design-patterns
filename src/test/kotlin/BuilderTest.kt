import junit.framework.Assert.assertEquals
import org.junit.Test

class BuilderTest {


    @Test
    fun `Build a House`() {
        val houseBuilder = HouseBuilder()

        val houseCreated = houseBuilder
            .withDoors(4)
            .withRooms(4)
            .withWindows(8)
            .withGarden()
            .build()

        assertEquals(House(8,4,4,false,false,true),houseCreated)
    }

    data class House(val windows: Int,
                val doors:Int,
                val rooms:Int,
                val hasGarage:Boolean,
                val hasSwimPool: Boolean,
                val hasGarden: Boolean)


    class HouseBuilder{

        private var windows: Int = 4
        private var doors: Int = 2
        private var rooms: Int = 3
        private var hasGarage = false
        private var hasSwimPool = false
        private var hasGarden = false

        fun withWindows(windows: Int):HouseBuilder{
            this.windows = windows
            return this
        }

        fun withDoors(doors: Int):HouseBuilder{
            this.doors = doors
            return this
        }

        fun withRooms(rooms: Int):HouseBuilder{
            this.rooms = rooms
            return this
        }

        fun withGarage(): HouseBuilder{
            this.hasGarage = true
            return this
        }

        fun withSwimPool(): HouseBuilder{
            this.hasSwimPool = true
            return this
        }

        fun withGarden(): HouseBuilder{
            this.hasGarden = true
            return this
        }

        fun build(): BuilderTest.House {
            return BuilderTest.House(windows,
                doors,
                rooms,
                hasGarage,
                hasSwimPool,
                hasGarden)
        }
    }
}