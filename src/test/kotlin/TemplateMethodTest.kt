import org.junit.Test

class TemplateMethodTest {

    @Test
    fun GameIA() {

        abstract class GameIA {

            fun turn() { // template method
                collectResources()
                buildStructures()
                buildUnits()
                attack()
            }

            abstract fun collectResources()

            abstract fun buildStructures()

            abstract fun buildUnits()

            fun attack() = println("POW POW")

        }

        class OrcsIA : GameIA() {
            override fun collectResources() {
                println("collecting resources")
            }

            override fun buildStructures() {
                println("building structures")
            }

            override fun buildUnits() {
                println("building units")
            }
        }

        class MonstersIA : GameIA() {
            override fun collectResources() {
            }

            override fun buildStructures() {
            }

            override fun buildUnits() {
            }

            fun resurrect() {
                println("i'm back")
            }
        }
    }

    @Test
    fun Drink() {

        abstract class Drink {
            fun prepareDrink() {
                boilWater()
                addIngredient()
                fillWaterInCup()
            }

            fun boilWater() = println("boiling water")

            abstract fun addIngredient()

            fun fillWaterInCup() = println("filling water")

        }

        class Coffee : Drink() {
            override fun addIngredient() {
                println("adding coffee")
            }

        }

        class Tea : Drink() {
            override fun addIngredient() {
                println("adding tea")
            }
        }
    }
}


