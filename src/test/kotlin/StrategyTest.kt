import org.junit.Test

class StrategyTest {

    @Test
    fun Navigation() {

        val strategy = WalkingStrategy()
        val navigator = Navigator(strategy)
        println(navigator.showRouteTime())






    }

    interface RouteStrategy{
        fun buildRoute():Route
    }

    class RoadStrategy: RouteStrategy{
        override fun buildRoute():Route {
            return Route("3 min with a car")
        }
    }

    class WalkingStrategy: RouteStrategy{
        override fun buildRoute():Route {
            return Route("15 min walking")
        }
    }

    class Bustrategy: RouteStrategy{
        override fun buildRoute():Route {
            return Route("7 min with a bus")
        }
    }

    class Route(val time: String)

    class Navigator(private var strategy: RouteStrategy){

        fun setStrategy(strategy: RouteStrategy){
            this.strategy = strategy
        }

        fun showRouteTime(): String{
            val route = strategy.buildRoute()
            return route.time
        }
    }
}